## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| attributes | Additional attributes (e.g. `1`) | list | `<list>` | no |
| delimiter | Delimiter to be used between `namespace`, `environment`, `name`, and `attributes` | string | `"-"` | no |
| environment | environment (e.g. `prod`, `dev`, `staging`, `infra`) | string | n/a | yes |
| iam\_policy\_arn\_count | The number of policies included in iam_policy_arns | string | n/a | yes |
| iam\_policy\_arns | List of IAM Policy ARNs to be attached to the role | list | n/a | yes |
| name | Name  (e.g. `app` or `cluster`) | string | n/a | yes |
| namespace | Namespace (e.g. `keanu`) | string | n/a | yes |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| instance\_role\_id |  |

