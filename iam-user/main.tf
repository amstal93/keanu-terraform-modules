resource "aws_iam_user" "default" {
  count                = var.enabled ? 1 : 0
  name                 = var.username
  path                 = var.path
  permissions_boundary = var.permissions_boundary
  force_destroy        = var.force_destroy
}

resource "aws_iam_user_login_profile" "default" {
  count = var.enabled && var.login_profile_enabled ? 1 : 0

  user                    = aws_iam_user.default[0].name
  pgp_key                 = var.pgp_key
  password_length         = var.password_length
  password_reset_required = var.password_reset_required
  depends_on              = [aws_iam_user.default]
}

resource "aws_iam_user_group_membership" "default" {
  count      = var.enabled ? 1 : 0
  user       = aws_iam_user.default[0].name
  groups     = var.groups
  depends_on = [aws_iam_user.default]
}

locals {
  encrypted_password = element(
    concat(
      aws_iam_user_login_profile.default.*.encrypted_password,
      [""],
    ),
    0,
  )
  gpg_password_pgp_message     = data.template_file.gpg_password_pgp_message.rendered
  gpg_password_decrypt_command = data.template_file.gpg_password_decrypt_command.rendered
  welcome_message              = data.template_file.welcome.rendered
}

data "template_file" "gpg_password_decrypt_command" {
  template = file("${path.module}/templates/gpg_password_decrypt_command.sh")

  vars = {
    encrypted_password = local.encrypted_password
  }
}

data "template_file" "gpg_password_pgp_message" {
  template = file("${path.module}/templates/gpg_password_pgp_message.txt")

  vars = {
    encrypted_password = local.encrypted_password
  }
}

data "template_file" "welcome" {
  template = file("${path.module}/templates/welcome.txt")

  vars = {
    encrypted_password       = local.encrypted_password
    signin_url               = var.signin_url
    username                 = var.username
    password_decrypt_command = local.gpg_password_decrypt_command
  }
}
