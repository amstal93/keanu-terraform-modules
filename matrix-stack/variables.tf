variable "region" {
  type = string
}

variable "cloudflare_zone_id" {
  type        = string
  description = "The zone id of the zone"
}

variable "availability_zone_1" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "vpc_cidr_block" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "security_group_id_general" {
  type = string
}

variable "key_name" {
  type = string
}

variable "domain_name" {
  type = string
}

variable "database_port" {
  type = string
}

variable "database_name" {
  type = string
}

variable "database_username" {
  type = string
}

variable "database_password" {
  type = string
}

variable "database_address" {
  type = string
}

variable "database_endpoint" {
  type = string
}

### label
variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `namespace`, `environment`, `name`, and `attributes`"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}

variable "namespace" {
  type        = string
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = string
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
}

variable "name" {
  type        = string
  description = "Name  (e.g. `app` or `cluster`)"
}

variable "aws_log_bucket" {
  type = string
}

variable "aws_log_bucket_log_prefix" {
  type = string
}

variable "session_manager_bucket" {
  type        = string
  description = "S3 bucket name of the bucket where SSM Session Manager logs are stored"
}

variable "node_exporter_port" {
  type        = string
  description = "The port which node exporter is exposed on"
  default     = "9100"
}

variable "smtp_login" {
}

variable "smtp_host" {
}

variable "smtp_port" {
}

variable "smtp_password" {
}

variable "smtp_from" {
}
