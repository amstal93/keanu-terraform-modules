variable "ami_synapse" {
  type = string
}

variable "instance_type_synapse" {
  type = string
}

variable "macaroon_secret_key" {
  type = string
}

variable "password_config_pepper" {
  type = string
}

variable "web_client_base_url" {
  type = string
}

variable "shared_registration_secret" {
  type = string
}

variable "synapse_signing_key" {
  type = string
}

variable "synapse_disk_allocation_gb" {
  # TODO(tf12) string to int
  type = string
}

variable "admin_contact_email" {
  type = string
}

variable "manhole_enabled" {
  description = "whether the local manhole is enabled or not, 'true' or 'false'"
  type        = string
}

variable "federation_enabled" {
  description = "whether federation is enabled or not, 'true' or 'false'"
  type        = string
}

variable "metrics_enabled" {
  description = "whether the metrics endpoint is enabled or not, 'true' or 'false'"
  type        = string
}

variable "metrics_bind_addresses" {
  description = "the interface addresses to bind to and expose metrics over"
  type        = list(string)
}

variable "federation_domain_whitelist" {
  type = list(string)
}
