# creates a lambda@edge lambda to redirect
# /usercontent/ to /usercontent/index.html
#
module "label_lambda" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = concat(var.attributes, ["lambda-directory-index"])
  tags        = var.tags
}
data "archive_file" "folder_index_redirect_zip" {
  type        = "zip"
  output_path = "${path.module}/folder_index_redirect.js.zip"
  source_file = "${path.module}/folder_index_redirect.js"
}

resource "aws_iam_role_policy" "lambda_execution" {
  name = module.label_lambda.id
  role = aws_iam_role.lambda_execution.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:CreateLogGroup"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "lambda_execution" {
  name               = module.label_lambda.id
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "edgelambda.amazonaws.com",
          "lambda.amazonaws.com"
        ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = var.tags
}

provider "aws" {
  region = "us-east-1"
  alias  = "aws_cloudfront"
}

resource "aws_lambda_function" "folder_index_redirect" {
  description      = "Redirects /sub/ to /sub/index.html"
  filename         = "${path.module}/folder_index_redirect.js.zip"
  function_name    = module.label_lambda.id
  handler          = "folder_index_redirect.handler"
  source_code_hash = data.archive_file.folder_index_redirect_zip.output_base64sha256
  provider         = aws.aws_cloudfront
  publish          = true
  role             = aws_iam_role.lambda_execution.arn
  runtime          = "nodejs12.x"
  tags             = module.label_lambda.tags
}
