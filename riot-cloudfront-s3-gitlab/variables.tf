variable "namespace" {
  type        = string
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = string
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `namespace`, `stage`, `name` and `attributes`"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "name" {
  type        = string
  description = "Name  (e.g. `app` or `cluster`)"
}

variable "domain_name" {
  type        = string
  description = "The domain name that the distribution will be served under (e.g., web.keanu.im)"
}

#variable "web_client_base_url" {
#  type = "string"
#}

variable "identity_server_url" {
  type        = string
  description = "The full URL to the default identity server"
}

variable "homeserver_url" {
  type        = string
  description = "The full URL to the default homeserver"
}

variable "cloudflare_zone_id" {
  type = string
}

variable "gitlab_project" {
  type        = string
  description = "The gitlab project that will deploy the app"
}

variable "iam_certificate_id" {
  type        = string
  description = "The IAM certificate identifier for serving the cloudfront distribution"
}

variable "origin_path" {
  # http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/distribution-web-values-specify.html#DownloadDistValuesOriginPath
  description = "An optional element that causes CloudFront to request your content from a directory in your Amazon S3 bucket or your custom origin. It must begin with a /. Do not add a / at the end of the path."
  default     = ""
}

variable "riot_config" {
  description = "The JSON config for riot"
  type        = string
}

variable "gitlab_var_name_bucket_name" {
  description = "The name of the variable in the gitlab project that determines the s3 bucket used"
  type        = string
}

variable "gitlab_var_name_aws_access_key_id" {
  description = "The name of the variable in the gitlab project that determines the s3 bucket used"
  type        = string
}

variable "gitlab_var_name_aws_secret_access_key" {
  description = "The name of the variable in the gitlab project that determines the s3 bucket used"
  type        = string
}

variable "gitlab_var_name_aws_default_region" {
  description = "The name of the variable in the gitlab project that determines the s3 bucket used"
  type        = string
}
