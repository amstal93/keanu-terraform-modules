output "region" {
  value = var.region
}

output "igw_id" {
  value       = aws_internet_gateway.default.id
  description = "The ID of the Internet Gateway"
}

output "vpc_id" {
  value       = aws_vpc.default.id
  description = "The ID of the VPC"
}

output "vpc_cidr_block" {
  value       = aws_vpc.default.cidr_block
  description = "The CIDR block of the VPC"
}

output "vpc_main_route_table_id" {
  value       = aws_vpc.default.main_route_table_id
  description = "The ID of the main route table associated with this VPC."
}

output "vpc_default_network_acl_id" {
  value       = aws_vpc.default.default_network_acl_id
  description = "The ID of the network ACL created by default on VPC creation"
}

output "vpc_default_security_group_id" {
  value       = aws_vpc.default.default_security_group_id
  description = "The ID of the security group created by default on VPC creation"
}

output "vpc_default_route_table_id" {
  value       = aws_vpc.default.default_route_table_id
  description = "The ID of the route table created by default on VPC creation"
}

output "vpc_ipv6_association_id" {
  value       = aws_vpc.default.ipv6_association_id
  description = "The association ID for the IPv6 CIDR block"
}

output "ipv6_cidr_block" {
  value       = aws_vpc.default.ipv6_cidr_block
  description = "The IPv6 CIDR block"
}

output "subnet_public_1_id" {
  value = aws_subnet.public_1.id
}

output "subnet_public_1_cidr_block" {
  value = aws_subnet.public_1.cidr_block
}

output "subnet_public_1_availability_zone" {
  value = aws_subnet.public_1.availability_zone
}

output "subnet_public_2_id" {
  value = aws_subnet.public_2.id
}

output "subnet_public_2_cidr_block" {
  value = aws_subnet.public_2.cidr_block
}

output "subnet_public_2_availability_zone" {
  value = aws_subnet.public_2.availability_zone
}

output "subnet_private_1_id" {
  value = aws_subnet.private_app_1.id
}

output "subnet_private_1_cidr_block" {
  value = aws_subnet.private_app_1.cidr_block
}

output "subnet_private_1_availability_zone" {
  value = aws_subnet.private_app_1.availability_zone
}

#output "subnet_private_2_id" {
#  value = "${aws_subnet.private_2.id}"
#}
#output "subnet_private_2_cidr_block" {
#  value = "${aws_subnet.private_2.cidr_block}"
#}
#output "subnet_private_2_availability_zone" {
#  value = "${aws_subnet.private_2.availability_zone}"
#}

output "subnet_rds_a_1_id" {
  value = aws_subnet.private_rds_a_1.id
}

output "subnet_rds_a_1_cidr_block" {
  value = aws_subnet.private_rds_a_1.cidr_block
}

output "subnet_rds_a_1_availability_zone" {
  value = aws_subnet.private_rds_a_1.availability_zone
}

output "subnet_rds_a_2_id" {
  value = aws_subnet.private_rds_a_2.id
}

output "subnet_rds_a_2_cidr_block" {
  value = aws_subnet.private_rds_a_2.cidr_block
}

output "subnet_rds_a_2_availability_zone" {
  value = aws_subnet.private_rds_a_2.availability_zone
}

output "subnet_rds_subnet_group" {
  value = aws_db_subnet_group.rds_subnets.id
}

output "nat_gw_eip_id" {
  value = aws_eip.nat_1.id
}

output "nat_gw_eip_public_ip" {
  value = aws_eip.nat_1.public_ip
}

output "nat_gw_id" {
  value = aws_nat_gateway.nat_1.id
}

output "sg_private_ssh_id" {
  value = aws_security_group.private_app_1.id
}
