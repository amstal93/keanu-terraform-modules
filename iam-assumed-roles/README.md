## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| admin\_group\_name | Name for the admin group and role (e.g. `admin`) | string | `"admin"` | no |
| admin\_user\_names | Optional list of IAM user names to add to the admin group | list | `<list>` | no |
| attributes | Additional attributes (e.g. `policy` or `role`) | list | `<list>` | no |
| delimiter | Delimiter to be used between `namespace`, `environment`, `name`, and `attributes` | string | `"-"` | no |
| enabled | Set to false to prevent the module from creating any resources | string | `"true"` | no |
| environment | environment (e.g. `prod`, `dev`, `staging`) | string | n/a | yes |
| namespace | Namespace (e.g. `keanu`) | string | n/a | yes |
| readonly\_name | Name for the readonly group and role (e.g. `readonly`) | string | `"readonly"` | no |
| readonly\_user\_names | Optional list of IAM user names to add to the readonly group | list | `<list>` | no |
| switchrole\_url | URL to the IAM console to switch to a role | string | `"https://signin.aws.amazon.com/switchrole?account=%s\u0026roleName=%s\u0026displayName=%s"` | no |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| group\_admin\_arn | Admin group ARN |
| group\_admin\_id | Admin group ID |
| group\_admin\_name | Admin group name |
| group\_readonly\_arn | Readonly group ARN |
| group\_readonly\_id | Readonly group ID |
| group\_readonly\_name | Readonly group name |
| role\_admin\_arn | Admin role ARN |
| role\_admin\_name | Admin role name |
| role\_readonly\_arn | Readonly role ARN |
| role\_readonly\_name | Readonly role name |
| switchrole\_admin\_url | URL to the IAM console to switch to the admin role |
| switchrole\_readonly\_url | URL to the IAM console to switch to the readonly role |

