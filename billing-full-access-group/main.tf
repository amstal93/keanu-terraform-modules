module "label" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = var.attributes
  tags        = var.tags
}

locals {
  enabled = var.enabled == "true" ? true : false
}

# https://www.terraform.io/docs/providers/aws/r/iam_group.html
resource "aws_iam_group" "default" {
  count = local.enabled ? 1 : 0
  name  = module.label.id
}

# https://www.terraform.io/docs/providers/aws/d/iam_policy_document.html
data "aws_iam_policy_document" "billing_full_access" {
  statement {
    actions = [
      "aws-portal:*",
    ]

    resources = [
      "*",
    ]

    effect = "Allow"
  }
}

resource "aws_iam_group_policy" "billing_full_access" {
  name   = module.label.id
  group  = join("", aws_iam_group.default.*.id)
  policy = data.aws_iam_policy_document.billing_full_access.json
}
