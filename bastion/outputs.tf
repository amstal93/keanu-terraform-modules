output "ssh_user" {
  value = var.ssh_user
}

output "security_group_id" {
  value = module.bastion_with_s3_keys.security_group_id
}

output "asg_id" {
  value = module.bastion_with_s3_keys.asg_id
}

output "bastion_host" {
  value = var.domain_name
}

output "bastion_public_ip" {
  value = aws_eip.bastion.public_ip
}
