if [ "${BASH_SOURCE[0]}" -ef "$0" ]
then
    echo "Hey, you should source this script, not execute it!"
    exit 1
fi

# Usage: use_tfenv [...]
#
# Load environment variables for a `terraform` project using `tfenv`.
# Any arguments given will be passed to `tfenv`.
#
function use_tfenv() {
  unset TF_CLI_ARGS_init
    # Export terraform environent
      eval "$(tfenv)"
}

# Usage: use_terraform [...]
#
# Load environment variables for a `terraform` project.
# Any arguments given will be passed to the terraform project.
# This should be called before `use tfenv`
# 
function use_terraform() {

  # The environment mapping below is not strictly necessary. Setting the `TF_CLI_*` envs is recommended.
  # The `TF_CLI_*` envs are handled by <https://github.com/cloudposse/tfenv>
  # This is done to preserve backwards compatibility with older environments and should eventually be deprecated.
  #
  # Terraform backend for a given project using envs
  unset TF_STATE_FILE
  export TF_STATE_FILE=${TF_FILE:-terraform.tfstate}
  export TF_BUCKET_REGION=${TF_BUCKET_REGION:-${AWS_REGION}}
  unset TF_BUCKET_PREFIX
  export TF_BUCKET_PREFIX=${TF_BUCKET_PREFIX:-$(basename $(pwd))}

  # Disable color if not running in a TTY (e.g. CI/CD context)
  if [ ! -t 1 ]; then
    export TF_CLI_DEFAULT_NO_COLOR=true
  fi

  # Translate environment variables to terraform arguments
  [ -z "${TF_FROM_MODULE}" ] || export TF_CLI_INIT_FROM_MODULE="${TF_FROM_MODULE}"
  [ -z "${TF_STATE_FILE}" ] || export TF_CLI_INIT_BACKEND_CONFIG_KEY="${TF_BUCKET_PREFIX}/${TF_STATE_FILE}"
  [ -z "${TF_BUCKET}" ] || export TF_CLI_INIT_BACKEND_CONFIG_BUCKET="${TF_BUCKET}"
  [ -z "${TF_BUCKET_REGION}" ] || export TF_CLI_INIT_BACKEND_CONFIG_REGION="${TF_BUCKET_REGION}"
  [ -z "${TF_DYNAMODB_TABLE}" ] || export TF_CLI_INIT_BACKEND_CONFIG_DYNAMODB_TABLE="${TF_DYNAMODB_TABLE}"
  [ -z "${AWS_PROFILE}" ] || export TF_CLI_INIT_BACKEND_CONFIG_PROFILE="${AWS_PROFILE}"
  [ -z "${AWS_ROLE_ARN}" ] || export TF_CLI_INIT_BACKEND_CONFIG_ROLE_ARN="${AWS_ROLE_ARN}"
}

function check_aws_role() {

  if [ -z "${1}" ]; then
    echo "usage: $0 <AWS_CONFIG_ROLE_NAME>"
    return 1
  fi
  echo "Checking AWS Role for profile $1"

  aws-vault exec "$1" -- aws sts get-caller-identity --output text --query 'Arn' \
    | sed 's/:sts:/:iam:/g' \
    | sed 's,:assumed-role/,:role/,' \
    | cut -d/ -f1-2 \
    | grep role
  rc=$?
  if [ "$rc" != "0" ]; then
    echo
    echo "Error: something went wrong. Does the profile '$1' exist?"
    return 1;
  fi
}
