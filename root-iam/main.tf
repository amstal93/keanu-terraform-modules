# Provision group access to root account with MFA
module "organization_access_group_root" {
  source              = "../iam-assumed-roles"
  namespace           = var.root_account_namespace
  environment         = var.environment
  admin_group_name    = var.admin_group_name
  readonly_name       = "readonly"
  admin_user_names    = var.root_account_admin_user_names
  readonly_user_names = var.root_account_readonly_user_names
}

module "organization_access_group_ssm_root" {
  source = "../ssm-parameter-store"

  parameter_write = [
    {
      name        = "/${var.root_account_namespace}/root/admin_group"
      value       = module.organization_access_group_root.group_admin_name
      type        = "String"
      overwrite   = "true"
      description = "IAM admin group name for the root account"
    },
    {
      name        = "/${var.root_account_namespace}/root/readonly_group"
      value       = module.organization_access_group_root.group_readonly_name
      type        = "String"
      overwrite   = "true"
      description = "IAM readonly group name for the root account"
    },
  ]
}

# Provision a group policy that allows full billing access
module "billing_full_access_group_root" {
  source      = "../billing-full-access-group"
  namespace   = var.root_account_namespace
  environment = var.environment
  name        = var.billing_group_name
  attributes  = ["full", "access"]
}

module "billing_full_access_group_ssm_root" {
  source = "../ssm-parameter-store"

  parameter_write = [
    {
      name        = "/${var.root_account_namespace}/root/billing_full_access_group"
      value       = module.billing_full_access_group_root.group_name
      type        = "String"
      overwrite   = "true"
      description = "IAM admin group name for the root account"
    },
  ]
}
