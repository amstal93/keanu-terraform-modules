output "admin_group" {
  value = module.organization_access_group_root.group_admin_name
}

output "admin_switchrole_url" {
  description = "URL to the IAM console to switch to the admin role"
  value       = module.organization_access_group_root.switchrole_admin_url
}

output "readonly_group" {
  value = module.organization_access_group_root.group_readonly_name
}

output "readonly_switchrole_url" {
  description = "URL to the IAM console to switch to the readonly role"
  value       = module.organization_access_group_root.switchrole_readonly_url
}

output "role_admin_arn" {
  description = "Admin role ARN"
  value       = module.organization_access_group_root.role_admin_arn
}

output "role_readonly_arn" {
  description = "Readonly role ARN"
  value       = module.organization_access_group_root.role_readonly_arn
}

output "billing_full_access_group" {
  value = module.billing_full_access_group_root.group_name
}
