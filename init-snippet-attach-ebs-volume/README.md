## Init Snippet: Attach EBS Volume

Create an init snippet that will attach an EBS volume to the instance.
This snippet requires that the instance has an IAM instance profile which
grants it the access needed to find and attach the EBS volume. There are
other modules in this repo which can create EBS volumes with IAM profiles
for each volume. Attaching the EBS volume will loop until it succeeds.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| device\_path | path, to the device's path in /dev/ | string | `"/dev/xvdf"` | no |
| init\_prefix | initial init (shellcode) to prefix this snippet with | string | `""` | no |
| init\_suffix | init (shellcode) to append to the end of this snippet | string | `""` | no |
| log\_level | default log level verbosity for apps that support it | string | `"info"` | no |
| log\_prefix | string to prefix log messages with | string | `"OPS: "` | no |
| region | AWS region the volume is in | string | n/a | yes |
| volume\_id | ID of the EBS volume to attach | string | n/a | yes |
| wait\_interval | time (in seconds) to wait when looping to find the device | string | `"5"` | no |

## Outputs

| Name | Description |
|------|-------------|
| init\_snippet |  |

