## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| account\_arn | The AWS sub account's ARN | string | n/a | yes |
| account\_id | The AWS sub account's id number | string | n/a | yes |
| accounts\_enabled | Accounts to enable | list | n/a | yes |
| environment | environment (e.g. `prod`, `dev`, `staging`) | string | n/a | yes |
| name | Application or solution name (e.g. `app`) | string | `"account"` | no |
| namespace | Namespace (e.g. `keanu`) | string | n/a | yes |
| organization\_account\_access\_role | The ARN of the OrganizationAccountAccessRole iam role tied to this subaccount | string | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| account\_arn |  |
| account\_id |  |
| organization\_account\_access\_role |  |
| switchrole\_url | URL to the IAM console to switch to the ${var.environment} account organization access role |

