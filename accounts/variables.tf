variable "namespace" {
  type        = string
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = string
  description = "environment (e.g. `prod`, `dev`, `staging`)"
}

variable "name" {
  type        = string
  description = "Application or solution name (e.g. `app`)"
  default     = "account"
}

variable "account_id" {
  description = "The AWS sub account's id number"
}

variable "account_arn" {
  description = "The AWS sub account's ARN"
}

variable "organization_account_access_role" {
  description = "The ARN of the OrganizationAccountAccessRole iam role tied to this subaccount"
}

variable "accounts_enabled" {
  type        = list(string)
  description = "Accounts to enable"
}
