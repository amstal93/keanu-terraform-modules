module "account_parameters" {
  source = "../ssm-parameter-store"

  parameter_write = [
    {
      name        = "/${var.namespace}/${var.environment}/account_id"
      value       = var.account_id
      type        = "String"
      overwrite   = "true"
      description = "AWS Account ID"
    },
    {
      name        = "/${var.namespace}/${var.environment}/account_arn"
      value       = var.account_arn
      type        = "String"
      overwrite   = "true"
      description = "AWS Account ARN"
    },
    {
      name        = "/${var.namespace}/${var.environment}/organization_account_access_role"
      value       = var.organization_account_access_role
      type        = "String"
      overwrite   = "true"
      description = "AWS Organization Account Access Role"
    },
  ]
}

module "admin_organization_access_group" {
  source            = "../organization-access-group"
  enabled           = contains(var.accounts_enabled, var.environment) == true ? "true" : "false"
  namespace         = var.namespace
  environment       = var.environment
  name              = "admin"
  member_account_id = var.account_id
}

module "organization_access_group_ssm" {
  source  = "../ssm-parameter-store"
  enabled = contains(var.accounts_enabled, var.environment) == true ? "true" : "false"

  parameter_write = [
    {
      name        = "/${var.namespace}/${var.environment}/admin_group"
      value       = module.admin_organization_access_group.group_name
      type        = "String"
      overwrite   = "true"
      description = "IAM admin group name for the '${var.environment}' account"
    },
  ]
}
