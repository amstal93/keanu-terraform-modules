## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| additional\_tag\_map | Additional tags for appending to each tag map | map | `<map>` | no |
| attributes | Additional attributes (e.g. `state`) | list | `<list>` | no |
| cloudwatch\_encryption\_enabled | (Optional) Encrypt log data. | string | `"true"` | no |
| cloudwatch\_log\_group\_name | (Optional) The name of the log group to upload session logs to. Specifying this enables sending session output to CloudWatch Logs. | string | `""` | no |
| context | Default context to use for passing state between label invocations | map | `<map>` | no |
| delimiter | Delimiter to be used between `namespace`, `environment`, `stage`, `name` and `attributes` | string | `"-"` | no |
| environment | Environment, e.g. 'prod', 'staging', 'dev' | string | `""` | no |
| force\_destroy | A boolean that indicates the S3 bucket can be destroyed even if it contains objects. These objects are not recoverable | string | `"false"` | no |
| label\_order | The naming order of the id output and Name tag | list | `<list>` | no |
| name | Solution name, e.g. 'neo' or 'jenkins' | string | `"terraform"` | no |
| namespace | Namespace, which could be your organization name or abbreviation, e.g. 'keanu' | string | `""` | no |
| region | AWS Region the S3 bucket should reside in | string | n/a | yes |
| s3\_encryption\_enabled | (Optional) Encrypt log data. | string | `"true"` | no |
| s3\_key\_prefix | (Optional) To write output to a sub-folder, enter a sub-folder name. | string | `""` | no |
| tags | Additional tags (e.g. `map('Foo','Bar')` | map | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| document\_arn | ARN of the created document. You can use this to create IAM policies that prevent changes to session manager preferences. |
| document\_name | Name of the created document. |
| s3\_bucket\_arn | S3 bucket ARN |
| s3\_bucket\_domain\_name | S3 bucket domain name |
| s3\_bucket\_id | S3 bucket ID |

