output "s3_bucket_domain_name" {
  value       = aws_s3_bucket.default.bucket_domain_name
  description = "S3 bucket domain name"
}

output "s3_bucket_id" {
  value       = aws_s3_bucket.default.id
  description = "S3 bucket ID"
}

output "s3_bucket_arn" {
  value       = aws_s3_bucket.default.arn
  description = "S3 bucket ARN"
}

output "document_name" {
  description = "Name of the created document."
  value       = aws_ssm_document.session_manager_prefs.name
}

output "document_arn" {
  description = "ARN of the created document. You can use this to create IAM policies that prevent changes to session manager preferences."
  value       = aws_ssm_document.session_manager_prefs.arn
}
